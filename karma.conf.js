// Karma configuration
// Generated on Sat Jul 30 2016 22:46:01 GMT+0200 (CEST)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
        './node_modules/phantomjs-polyfill/bind-polyfill.js', // needed for phantomjs
        './node_modules/jquery/dist/jquery.js',
        './node_modules/angular/angular.js',
        './node_modules/angular-mocks/angular-mocks.js',
        './node_modules/angular-ui-router/release/angular-ui-router.js',
        './node_modules/angular-bootstrap/ui-bootstrap-tpls.js',
        './node_modules/angular-resource/angular-resource.js',
        './node_modules/angular-animate/angular-animate.js',
        './node_modules/angularjs-toaster/toaster.js',
        './node_modules/angular-timer/dist/angular-timer.js',
        'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.js',
        './node_modules/humanize-duration/humanize-duration.js',
        './app/static/dist/js/app.js',
        './app/tests.webpack.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
        './app/tests.webpack.js': ['webpack']
    },


    webpack: require('./webpack.config.js'),
    

    webpackMiddleware: {
      noInfo: 'errors-only'
    },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    // logLevel: config.LOG_INFO,
    logLevel: config.LOG_DEBUG,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  });
};
