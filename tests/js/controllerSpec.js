describe('Controller: ', function() {

    var ctrl, scope, mockService;
    
    beforeEach(function() {
        // u cant use module('myApp') because it seems that webpack overwrites it,
        // so u need to use full name
        angular.mock.module('myApp');
    });

    // mock Service
    beforeEach(angular.mock.module(function($provide) {
        mockService = {
            getList: function() {
                return [1,2];
            },
            deleteSeries: function() {},
            createSeries: function() { return 2; },
            addSeries: function(a,b) { return [a,b]; },
            updateSeries: function(data, air_time) {},
            getSeries: function(id, cb) {
                data = {};
                data.data = {
                    next_episode: { air_time: '20:00:00' }
                };
                cb(data);
            }
        };
        $provide.value('Series', mockService);
    }));

    describe('SeriesListController', function() {

        beforeEach(inject(function($rootScope, $controller) {
            scope = $rootScope.$new();
            ctrl = $controller('SeriesListController', { $scope: scope });
        }));


        it('should have array of series in the scope', function() {
            expect(scope.series_array).toEqual([1,2]);
        });

        it('should have deleteSeries in the scope', function() {
            expect(scope.deleteSeries.constructor).toBe(Function);
        });
    });

    describe('SeriesCreateController', function() {

        var Series;

        beforeEach(inject(function($rootScope, $controller, _Series_) {
            spyOn(_Series_, 'createSeries').and.callThrough();
            spyOn(_Series_, 'addSeries').and.callThrough();
            Series = _Series_;
            scope = $rootScope.$new();
            ctrl = $controller('SeriesCreateController', { $scope: scope });
        }));
        
        it('should have scope.series set to SeriesApi', function() {
            expect(scope.series).toEqual(2);
            expect(Series.createSeries).toHaveBeenCalledWith();
        });

        it('should have scope.addSeries which gets result from Series.addSeries', function() {
            expect(scope.addSeries.constructor).toBe(Function);
            var res = scope.addSeries();
            expect(Series.addSeries).toHaveBeenCalledWith(scope.series, scope.episode_air_time);
            expect(res).toEqual([scope.series, scope.episode_air_time]);
        });

    });

    describe('SeriesEditController', function() {
        
        var Series;

        // mock $stateParams
        beforeEach(angular.mock.module(function($provide) {
            $provide.value('$stateParams', { id: 1 });
        }));

        beforeEach(inject(function($rootScope, $controller, _Series_) {
            spyOn(_Series_, 'getSeries').and.callThrough();
            spyOn(_Series_, 'updateSeries').and.callThrough();
            Series = _Series_;
            scope = $rootScope.$new();
            ctrl = $controller('SeriesEditController', { $scope: scope });
        }));

        it('should call Series.getSeries with correct id', function() {
            expect(Series.getSeries).toHaveBeenCalledWith(1, jasmine.any(Function));
        });

        it('should have scope.updateSeries, scope.episode_air_time and scope.series', function() {
            expect(scope.updateSeries).toBeDefined();
            expect(scope.updateSeries.constructor).toBe(Function);
            expect(scope.series).toBeDefined();
            expect(scope.episode_air_time instanceof Date).toBeTruthy();
        });

        it('should call Series.updateSeries when scope.updateSeries is called', function() {
            scope.updateSeries();
            expect(Series.updateSeries).toHaveBeenCalledWith({ data: scope.series }, scope.episode_air_time);
        });

    });
});

