from app import create_app
from app.models import db
from app.models import Episode, Season, Series
from app.testing import initTestingDB
from flask_testing import TestCase
from freezegun import freeze_time

import datetime

class TestSeasonModel(TestCase):

    def create_app(self):
        return create_app('testconfig')

    def setUp(self):
        db.create_all()
        initTestingDB(series=True)

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_add(self):
        series = Series.query.get(1)
        seasons_before = len(series.seasons)
        new_season = Season(
            season_nr=max([season.season_nr for season in series.seasons]) + 1,
            air_time=datetime.time(20,0,0),
            episodes=[
                Episode(
                    title='added_ep',
                    rating=1.2,
                    imdb_id='new_ep_imdb_id',
                    episode_nr=1,
                    air_date=datetime.date(2015,1,1)
                )
            ]
        )
        series.seasons.append(new_season)
        db.session.add(series)
        db.session.commit()
        series = Series.query.get(1)
        self.assertEqual(len(series.seasons), seasons_before + 1)
        saved_season = series.seasons[seasons_before]
        self.assertEqual(new_season.season_nr, saved_season.season_nr)
        self.assertEqual(new_season.air_time, saved_season.air_time)
        self.assertEqual(len(new_season.episodes), len(saved_season.episodes))
