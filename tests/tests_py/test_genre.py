from app import create_app
from app.models import db
from app.models import Series, Genre
from app.testing import initTestingDB
from flask import json
from flask_testing import TestCase
from sqlalchemy.exc import IntegrityError

class TestGenreModel(TestCase):

    def create_app(self):
        return create_app('testconfig')

    def setUp(self):
        db.create_all()
        initTestingDB(series=True)

    def tearDown(self):
        db.session.remove();
        db.drop_all()

    def test_add(self):
        before = len(Genre.query.all())
        new_genre = Genre('Foo')
        db.session.add(new_genre)
        db.session.commit()
        after = len(Genre.query.all())
        self.assertEqual(before, after-1)
        self.assertEqual(new_genre.name, Genre.query.get(new_genre.id).name)

    def test_add_duplicate_name(self):
        first_genre = Genre.query.first()
        new_genre = Genre(first_genre.name)
        with self.assertRaises(IntegrityError):
            db.session.add(new_genre)
            db.session.commit()


class TestGenreResourceApi(TestCase):

    def create_app(self):
        return create_app('testconfig')

    def setUp(self):
        db.create_all()
        initTestingDB(series=True)

    def tearDown(self):
        db.session.remove();
        db.drop_all()

    def test_series_genre_api(self):
        genres = Series.query.get(1).genres
        rv = self.client.get('/api/v1/series/1/genres')
        res = [{ 'name': genre.name, 'id': genre.id, } for genre in genres]
        js = json.loads(rv.data.decode())
        self.assertEqual(js['data'], res)
