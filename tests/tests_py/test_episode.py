from app import create_app
from app.models import db
from app.models import Episode, EpisodeSchema
from app.models import Season
from app.models import Series
from app.testing import initTestingDB
from flask_testing import TestCase
from sqlalchemy.exc import IntegrityError

import datetime

class TestEpisodeModel(TestCase):

    def create_app(self):
        return create_app('testconfig')

    def setUp(self):
        db.create_all()
        initTestingDB(series=True)

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_add(self):
        season = Season.query.get(1)
        last_episode=max([episode.episode_nr for episode in season.episodes])
        new_episode = Episode(
            title='new foo',
            rating=1.0,
            imdb_id='new_episode_imdb_id',
            episode_nr=last_episode+1,
            air_date=datetime.date(2015,1,1)
        )
        season.episodes.append(new_episode)
        db.session.add(season)
        db.session.commit()
        season = Season.query.get(1)
        saved_episode = season.episodes[-1]
        self.assertEqual(saved_episode.episode_nr, new_episode.episode_nr)
        self.assertEqual(saved_episode.title, 'new foo')
        self.assertEqual(saved_episode.rating, 1.0)
        self.assertEqual(saved_episode.imdb_id, 'new_episode_imdb_id')
        self.assertEqual(saved_episode.air_date, datetime.date(2015,1,1))


    def test_duplicate_episode_nr_same_season(self):
        season = Season.query.get(1)
        new_episode = Episode(
            title='new foo',
            rating=1.0,
            imdb_id='new_episode_imdb_id',
            episode_nr=season.episodes[0].episode_nr,
            air_date=datetime.date(2015,1,1)
        )
        season.episodes.append(new_episode)
        with self.assertRaises(IntegrityError):
            db.session.add(season)
            db.session.commit()

    def test_duplicate_episode_nr_different_season(self):
        # count on series with id=2 to have 2 seasons, first has more
        # episodes than the second
        series = Series.query.get(2)
        last_episode = max([ep.episode_nr for ep in series.seasons[1].episodes])
        new_episode = Episode(
            title='new foo',
            rating=1.0,
            imdb_id='new_episode_imdb_id',
            episode_nr=last_episode+1,
            air_date=datetime.date(2015,1,1)
        )
        series.seasons[1].episodes.append(new_episode)
        try:
            db.session.add(series)
            db.session.commit()
            series = Series.query.get(2)
            self.assertEqual(
                max([ep.episode_nr for ep in series.seasons[1].episodes]),
                last_episode+1
            )
        except Exception as e:
            self.fail('Adding duplicate episode_nr different season raised:{}'.format(e))

    def test_duplicate_imdb_id(self):
        season = Season.query.get(1)
        last_episode=max([episode.episode_nr for episode in season.episodes])
        new_episode = Episode(
            title='new foo',
            rating=1.0,
            imdb_id=season.episodes[0].imdb_id,
            episode_nr=last_episode+1,
            air_date=datetime.date(2015,1,1)
        )
        season.episodes.append(new_episode)
        with self.assertRaises(IntegrityError):
            db.session.add(season)
            db.session.commit()

    def test_schema_dump_has_next_episode(self):
        episode = Episode.query.get(1)
        dumped = EpisodeSchema(strict=True).dump(episode).data
        self.assertIsNotNone(dumped.get('air_time'))
