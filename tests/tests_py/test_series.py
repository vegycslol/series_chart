from app import create_app
from app.models import db
from app.models import Episode, EpisodeSchema
from app.models import Genre, GenreSchema
from app.models import Season, SeasonSchema
from app.models import Series, SeriesSchema
from app.testing import initTestingDB
from flask_testing import TestCase
from freezegun import freeze_time
from sqlalchemy.exc import IntegrityError

import datetime
import json

class TestSeriesModel(TestCase):

    def create_app(self):
        return create_app('testconfig')

    def setUp(self):
        db.create_all()
        initTestingDB(series=True)
        self.new_series = Series(
            title='TestAdd',
            description='TestAddDescription',
            actors='TestAddActor1, TestAddActor2',
            poster='test_add.png',
            imdb_id='test_add_imdb_id',
            rating=5.8,
            seasons=[
                Season(
                    season_nr=1,
                    air_time=datetime.time(20,0,0),
                    episodes=[
                        Episode(
                            imdb_id='imdb_id_test_add_ep1',
                            title='test_add_ep1',
                            rating=9.2,
                            episode_nr=1,
                            air_date=datetime.date(2016,2,3)
                        )
                    ]
                ),
                Season(
                    season_nr=2,
                    air_time=datetime.time(19,0,0),
                    episodes=[
                        Episode(
                            imdb_id='imdb_id_test_add_ep2',
                            title='test_add_ep2',
                            rating=2.3,
                            episode_nr=2,
                            air_date=datetime.date(2016,2,10)
                        )
                    ]
                )
            ],
            genres=[Genre.query.get(1), Genre.query.get(2)]
        )

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_add(self):
        new_series = self.new_series
        new_series.add()
        saved_series = Series.query.get(new_series.id)
        check_attrs = ['title', 'description', 'actors', 'poster', 'imdb_id', 'rating']
        check_refs = ['seasons', 'genres']
        for attr in check_attrs:
            self.assertEqual(getattr(saved_series, attr), getattr(new_series, attr))
        for ref in check_refs:
            self.assertEqual(
                [x.id for x in getattr(saved_series, ref)],
                [x.id for x in getattr(new_series, ref)]
            )

    def test_add_duplicate_imdb_id(self):
        series = Series.query.get(1)
        self.new_series.imdb_id = series.imdb_id
        with self.assertRaises(IntegrityError):
            self.new_series.add()

    def test_remove(self):
        series = Series.query.get(1)
        seasons_ids = [season.id for season in series.seasons]
        genre_ids = [genre.id for genre in series.genres]
        result = db.engine.execute("select * from genre_series")
        self.assertIn(1, [series_id for series_id, _ in result])
        series.delete()
        self.assertIsNone(Series.query.get(1))
        # check that seasons are deleted
        self.assertEqual(Season.query.filter_by(series_id=1).all(), [])
        # check that episodes are deleted
        self.assertEqual(
            Episode.query.filter(Episode.season_id.in_(seasons_ids)).all(),
            []
        )
        # check that entries from table genre_series were deleted
        result = db.engine.execute("select * from genre_series")
        self.assertNotIn(1, [series_id for series_id, _ in result])
        # check that genres were not removed
        self.assertIsNotNone(Genre.query.get(genre_ids[0]))


    def test_update(self):
        # relies on series with id=1 to have 1 season and 2 genres
        series = Series.query.get(1)
        check_attrs = ['title', 'description', 'actors', 'poster', 'imdb_id', 'rating']
        for attr in check_attrs:
            value = 'updated_' + attr
            if attr == 'rating':
                value = 0.1
            setattr(series, attr, value)
        new_season = Season(
            season_nr=2,
            air_time=datetime.time(20,0,0),
            episodes=[
                Episode(
                    imdb_id='imdb_id_updated_ep1',
                    title='updated_ep1',
                    rating=9.2,
                    episode_nr=1,
                    air_date=datetime.date(2016,2,2)
                ),
            ]
        )
        series.seasons.append(new_season)
        series.genres.pop()
        series.update()
        saved_series = Series.query.get(1)
        for attr in check_attrs:
            self.assertEqual(getattr(saved_series, attr), getattr(series, attr))
        self.assertEqual(len(saved_series.seasons), 2)
        self.assertEqual(len(saved_series.genres), 1)

    def test_schema_dump_has_next_episode(self):
        series = Series.query.get(1)
        dumped = SeriesSchema(strict=True).dump(series).data
        self.assertIsNotNone(dumped.get('next_episode'))

    @freeze_time('3000-01-01')
    def test_get_next_episode_finished(self):
        series = Series.query.get(1)
        self.assertIsNone(series.get_next_episode())

    def test_get_next_episode_no_episodes(self):
        series = Series.query.get(1)
        for season in series.seasons:
            season.episodes = []
        self.assertIsNone(series.get_next_episode())

    @freeze_time('2016-02-09')
    def test_get_next_episode_today(self):
        series = Series.query.get(1)
        self.assertEqual(
            series.get_next_episode(),
            series.seasons[0].episodes[1]
        )

    def test_get_next_episode_tomorrow(self):
        series = Series.query.get(1)
        self.assertEqual(
            series.get_next_episode(),
            series.seasons[0].episodes[-1]
        )


class TestSeriesResourceApi(TestCase):

    def create_app(self):
        return create_app('testconfig')

    def setUp(self):
        db.create_all()
        initTestingDB(series=True)

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get_series_list(self):
        rv = self.client.get('/api/v1/series/')
        series_list = json.loads(rv.data.decode())['data']
        self.assertEqual(len(series_list), 2)
        series2 = Series.query.get(2)
        rv_series2 = series_list[1]
        check_attrs = ['title', 'description', 'actors', 'poster', 'imdb_id', 'rating']
        for attr in check_attrs:
            self.assertEqual(rv_series2[attr], getattr(series2, attr))
        self.assertEqual(len(rv_series2['seasons']), len(series2.seasons))
        self.assertEqual(len(rv_series2['genres']), len(series2.genres))

    def test_get_series(self):
        rv = self.client.get('/api/v1/series/2')
        rv_series = json.loads(rv.data.decode())['data']
        series2 = Series.query.get(2)
        check_attrs = ['title', 'description', 'actors', 'poster', 'imdb_id', 'rating']
        for attr in check_attrs:
            self.assertEqual(rv_series[attr], getattr(series2, attr))
        self.assertEqual(len(rv_series['seasons']), len(series2.seasons))
        self.assertEqual(len(rv_series['genres']), len(series2.genres))
        # also must have 'next_episode'
        self.assertIn('next_episode', rv_series)

    def test_post_series(self):
        new_series = {
            'title': 'TestAdd',
            'description': 'TestAddDescription',
            'actors': 'TestAddActor1, TestAddActor2',
            'poster': 'test_add.png',
            'imdb_id': 'test_add_imdb_id',
            'rating': 5.8,
            'seasons': [
                {
                    'season_nr': 1,
                    'air_time': '20:00:00',
                    'episodes': [
                        {
                            'imdb_id': 'imdb_id_test_add_ep1',
                            'title': 'test_add_ep1',
                            'rating': 9.2,
                            'episode_nr': 1,
                            'air_date': '2016-02-03'
                        }
                    ]
                },
                {
                    'season_nr': 2,
                    'air_time': '19:00:00',
                    'episodes': [
                        {
                            'imdb_id': 'imdb_id_test_add_ep2',
                            'title': 'test_add_ep2',
                            'rating': 2.3,
                            'episode_nr': 2,
                            'air_date': '2016-02-10'
                        }
                    ]
                }
            ],
            'genres': [Genre.query.get(1).name, Genre.query.get(2).name]
        }
        rv = self.client.post(
            '/api/v1/series/',
            data=json.dumps(new_series),
            content_type='application/json'
        )
        rv_series = json.loads(rv.data.decode())['data']
        # check that everything has been added
        check_attrs = ['title', 'description', 'actors', 'poster', 'imdb_id', 'rating']
        for attr in check_attrs:
            self.assertEqual(rv_series[attr], new_series[attr])
        self.assertEqual(len(rv_series['seasons']), len(new_series['seasons']))
        self.assertEqual(len(rv_series['genres']), len(new_series['genres']))

    def test_update_series(self):
        new_attrs = {}
        new_attrs['data'] = {
            'title': 'Boo',
            'actors': 'BooActor1, BooActor2',
            'imdb_id': 'boo_imdb_id',
            'description': 'BooDescription',
            'genres': ['Horror', 'Animation'],
            'rating': 8.8
        }
        rv = self.client.patch(
            '/api/v1/series/1',
            data=json.dumps(new_attrs),
            content_type='application/json'
        )

        rv_series = json.loads(rv.data.decode())['data']
        series1 = Series.query.get(1)
        series1_data = SeriesSchema(strict=True).dump(series1).data
        for attr in new_attrs['data']:
            self.assertEqual(rv_series[attr], series1_data[attr])

    def test_delete_series(self):
        rv = self.client.delete('/api/v1/series/1')
        self.assertEqual(Series.query.get(1), None)
