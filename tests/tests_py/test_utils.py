from app import create_app
from app.models import Series
from flask_testing import TestCase
from marshmallow import ValidationError


class TestGetStringValidator(TestCase):

    def create_app(self):
        return create_app('testconfig')

    def test_string_validator(self):
        from app.models import get_string_validator
        validator = get_string_validator(Series, 'description')
        self.assertEqual(validator.min, 1)
        self.assertEqual(validator.max, 250)


class TestMustNotBeBlankValidator(TestCase):

    def create_app(self):
        return create_app('testconfig')

    def test_must_not_be_blank(self):
        from app.models import must_not_be_blank
        with self.assertRaises(ValidationError):
            must_not_be_blank([])
