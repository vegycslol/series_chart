var path = require('path');

/**
 * Env
 * Get npm lifecycle event to identify the environment
 * basically the name of the npm script that youre running is what ENV is
 */
var ENV = process.env.npm_lifecycle_event;
var isTest = ENV === 'test' || ENV === 'test-watch';


module.exports = {
    entry: isTest ? {} : {
        app: [
            './node_modules/angular-ui-router/release/angular-ui-router.js',
            './node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
            './node_modules/angular-resource/angular-resource.js',
            './node_modules/angular-animate/angular-animate.js',
            './node_modules/angularjs-toaster/toaster.js',
            './node_modules/angular-timer/dist/angular-timer.js',
            './app/static/js/app.js'
        ]
    },
    output: isTest ? {} : {
        filename: '[name].js',
        path: './app/static/dist/js'
    },
    module: {
        loaders: [
            {
                test: /\.css$/,
                include: path.resolve('app'),
                loader: 'style!css'
            }
        ]
    },
    externals: {
        angular: true,
        jquery: true,
        moment: true,
        humanizeDuration: true
    }
};
