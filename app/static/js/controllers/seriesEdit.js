var $inject = ['$scope', '$stateParams', 'Series'];

var SeriesEditController = function($scope, $stateParams, Series) {
    $scope.episode_air_time = new Date();
    var cb = function(data) {
        $scope.series = data.data;
        var time = $scope.series.next_episode !== null ?
            $scope.series.next_episode.air_time :
            $scope.series.seasons[$scope.series.seasons.length-1].air_time;
        time = time.split(':');
        // this new Date() is needed, seems like watchers need some sort of
        // assignment to be triggered
        $scope.episode_air_time = new Date();
        $scope.episode_air_time.setHours(parseInt(time[0]));
        $scope.episode_air_time.setMinutes(parseInt(time[1]));
        $scope.updateSeries = function() {
            Series.updateSeries(data, $scope.episode_air_time);
        };
    };
    Series.getSeries($stateParams.id, cb);

};

SeriesEditController.$inject = $inject;

module.exports = SeriesEditController;
