var $inject = ['$scope', 'Series'];

var SeriesListController = function($scope, Series) {
    $scope.series_array = Series.getList();
    $scope.deleteSeries = Series.deleteSeries;
};

SeriesListController.$inject = $inject;

module.exports = SeriesListController;
