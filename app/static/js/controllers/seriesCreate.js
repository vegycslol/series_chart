var $inject = ['$scope', 'Series'];

var SeriesCreateController = function($scope, Series) {
    // when u create a new series, u basically create a new instance of Resource, which is an empty object whose
    // prototype is an object that contains $delete, $get, $remove, $save etc + $update which u defined in the service
    // object gets populated with 2-way bindings inside form.html
    $scope.series = Series.createSeries();
    // set default time and imdb_id
    var d = new Date();
    d.setHours(20);
    d.setMinutes(0);
    $scope.episode_air_time = d;
    $scope.series.imdb_id = 'tt1520211';
    // partially apply series to addSeries
    $scope.addSeries = Series.addSeries.bind(undefined, $scope.series, $scope.episode_air_time);
};

SeriesCreateController.$inject = $inject;

module.exports = SeriesCreateController;
