var $inject = ['$state', '$stateParams', '$q', 'SeriesApi', 'omdbApi', 'toaster'];

var Series = function($state, $stateParams, $q, SeriesApi, omdbApi, toaster) {
    (function() {
        // helperFunctions contains functions which are used by the public
        // service functions
        var helperFunctions = {

            '_addEpisodes': function(season, episodes) {

                // function for renaming objects key
                var changeKey = function(o, old_key, new_key) {
                    Object.defineProperty(o, new_key, Object.getOwnPropertyDescriptor(o, old_key));
                    delete o[old_key];
                };

                // pairs of keys for renaming
                var rename_pairs = [
                    // first element is the old name, second is the new one
                    ['Episode', 'episode_nr'],
                    ['Released', 'air_date'],
                    ['Title', 'title'],
                    ['imdbID', 'imdb_id'],
                    ['imdbRating', 'rating']
                ];
                // change episodes keys
                for (var i = 0; i < episodes.length; i++) {
                    // the (notice that necessary 'the', otherwise its 
                    // considered a jshint command LOL :D) jshint doesnt like 
                    // that a new fn is created for each element, even though
                    // V8 usually uses the same fn, so ignore jshint
                    /* jshint ignore:start */
                    rename_pairs.forEach(function(element) {
                        changeKey(episodes[i], element[0], element[1]);
                    });
                    /* jshint ignore:end */
                    // set default for those without values
                    if(episodes[i].rating === 'N/A')
                        episodes[i].rating = -1.0;
                    if(episodes[i].air_date === 'N/A')
                        episodes[i].air_date = null;
                }
                season.episodes = episodes;
            },

            '_setBasicSeriesInfo': function(series, seriesInfo) {
                
                // check if series was found
                if (seriesInfo.Response === 'False') {
                    console.log(seriesInfo.error);
                    toaster.pop({
                        type: 'error',
                        title: 'Error',
                        body: 'OmdbApi is currently down.'
                    });
                    return;
                }
                // store series data
                series.title = seriesInfo.Title;
                series.genres = seriesInfo.Genre.split(',');
                series.description = seriesInfo.Plot;
                series.poster = seriesInfo.Poster;
                series.rating = seriesInfo.imdbRating !== 'N/A' ? parseFloat(seriesInfo.imdbRating) : -1.0;
                series.actors = seriesInfo.Actors;
            },

            '_setSeasonsInfo': function(series, totalSeasons, ep_air_time) {

                var seasonsInfo = [];
                for (var i = 1; i <= totalSeasons; i++) {
                    seasonsInfo.push(omdbApi.getSeasonInfo(series.imdb_id, i));
                }
                return $q.all(seasonsInfo).then(function(seasonsData) {
                    series.seasons = [];
                    angular.forEach(seasonsData, function(value, key) {
                        // check if season was loaded successfuly
                        if (value.status !== 200) {
                            toaster.pop({
                                type: 'error',
                                title: 'Error',
                                body: value.statusText
                            });
                        }
                        var cur_season = {
                            'season_nr': value.data.Season,
                            'air_time': ep_air_time.toTimeString().substring(0,6) + "00",
                            'episodes': []
                        };
                        helperFunctions._addEpisodes(cur_season, value.data.Episodes);
                        series.seasons.push(cur_season);
                    });
                    return series;
                });
            },

            '_set_series_info': function(series, ep_air_time) {

                var imdb_id = series.imdb_id;
                // wait for info about series and the seasons info
                var q = $q.all([
                    omdbApi.getSeriesInfo(imdb_id),
                    omdbApi.getSeasonInfo(imdb_id, 1)
                ])
                    .then(function(data) {

                        helperFunctions._setBasicSeriesInfo(series, data[0].data);
                        var s = helperFunctions._setSeasonsInfo(
                            series, data[1].data.totalSeasons, ep_air_time);
                        return s;
                    }, function() {
                            toaster.pop({
                                type: 'error',
                                title: 'Error',
                                body: 'Invalid info or API down'
                            });
                    });
                return q;
            },

            '_save': function(series) {
                series.$save(function() {
                    toaster.pop({
                        type: 'success',
                        title: 'Success',
                        body: 'Series saved successfully'
                    });

                    $state.go('series.list'); // on success go home
                }, function(data) {
                    toaster.pop({
                        type: 'error',
                        title: 'Error',
                        body: data.data.error
                    });
                });
            },

            '_update': function(series) {
                series.$update({
                    id: $stateParams.id
                }, function() {
                    toaster.pop({
                        type: 'success',
                        title: 'Success',
                        body: "Update was a success"
                    });

                    $state.go('series.list'); 

                }, function(error) {
                    toaster.pop({
                        type: 'error',
                        title: 'Error',
                        body: error
                    });
                });
            }
        };

        this.getList = function() {
            var series_array = [];
            SeriesApi.get(function(data) {
                angular.forEach(data.data, function(value, key) {
                    this.series = value;
                    this.series.genres = this.series.genres.map(
                        function(obj) {
                            return obj.name;
                        }
                    ).join(', ');
                    // shorten description
                    var description_max_length = 100;
                    if(this.series.description.length > description_max_length) {
                        this.series.description = this.series.description
                            .slice(0, description_max_length) // get first n chars
                            .split(' ') // split to get words
                            .slice(0, -1) // ignore the last word because it may be cut
                            .join(' ') + ' ...'; // join the words and add dots
                    }
                    // TODO: test all cases
                    this.series.ongoing = false;
                    if(this.series.next_episode !== null) {
                        var ii = this.series.next_episode.air_date + "T" + this.series.next_episode.air_time + ".000Z";
                        var date = new Date(ii); 
                        this.series.countdown = Math.abs(date);
                        
                        var limit = new Date();
                        limit.setMonth(limit.getMonth() + 1);
                        if(new Date(this.series.next_episode.air_date) < limit) {
                            this.series.ongoing = true;
                        }
                    }
                    this.push(this.series);

                }, series_array);
            });
            return series_array;
        };

        this.createSeries = function() {
            return new SeriesApi();
        };


        this.addSeries = function(series, ep_air_time) {
            var series_promise = helperFunctions._set_series_info(series, ep_air_time);
            series_promise.then(helperFunctions._save);
        };

        this.deleteSeries = function(id) { // Issues a DELETE to /api/v1/series/{series-id}.json

            SeriesApi.delete({
                'id': id
            }, function() {
                toaster.pop({
                    type: 'success',
                    title: 'Success',
                    body: "Series deleted successfully"
                });
                $state.go('series.list'); //redirect to home on a successful deletion
                $state.reload();

            }, function(error) {
                toaster.pop({
                    type: 'error',
                    title: 'Error',
                    body: error
                });
            });
        };

        this.getSeries = function(id, callback) {
            var series = SeriesApi.get({
                    'id': id
                },
                callback, function(error) {
                    toaster.pop({
                        type: 'error',
                        title: 'Error',
                        body: error
                    });
                });
            return series;
        };

        this.updateSeries = function(series_resource, ep_air_time) {
            var series_promise = helperFunctions._set_series_info(series_resource.data, ep_air_time);
            // actually ignores the value returned from promise because its already included in the resource
            // and we also need the resource
            series_promise.then(helperFunctions._update.bind(undefined, series_resource));
        };
    }).bind(this)();
};

Series.$inject = $inject;

module.exports = Series;
