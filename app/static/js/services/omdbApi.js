var $inject = ['$http'];
var omdbApi = function($http) {
    var omdbUrl = 'http://www.omdbapi.com/';

    this.getSeriesInfo = function(imdb_id) {
        return $http.get(omdbUrl, { params: { 'i': imdb_id } });
    };

    this.getSeasonInfo = function(imdb_id, season) {
        return $http.get(
            omdbUrl,
            { params: { 'i': imdb_id, 'Season': season } }
        );
    };
};

omdbApi.$inject = $inject;

module.exports = omdbApi;
