// Create a Series Resource Object using the resource service
// more on how to use $resource: http://stackoverflow.com/questions/13269882/angularjs-resource-restful-example
var $inject = ['$resource'];

var SeriesApi = function($resource) {
    return $resource('/api/v1/series/:id', {
        id: '@series.id' // this means that the default id will be data.series.id
    }, {
        update: { // action named 'update' uses PATCH method, resource gets a new method $update ($delete, $save, $get, $remove are built-in)
            method: 'PATCH',
        }
    }, {
        stripTrailingSlashes: false
    });
};

SeriesApi.$inject = $inject;

module.exports = SeriesApi;
