require('../css/style.css');

var app = angular.module(
    'myApp', 
    [
        'ui.router',
        'ngResource',
        'toaster',
        'ngAnimate',
        'ui.bootstrap',
        'timer'
    ]
);


// Create routes with UI-Router and display the appropriate HTML file for listing, adding or updating series
app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
           function($stateProvider, $urlRouterProvider, $locationProvider) {
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/");

    $stateProvider
        .state('series', {
            // https://github.com/angular-ui/ui-router/wiki/Nested-States-and-Nested-Views
            abstract: true,
            url: '/',
            title: 'Series',
            template: '<ui-view>'
        })
        .state('series.list', {
            url: '',
            templateUrl: 'list.html',
            controller: 'SeriesListController',
        })
        .state('series.add', {
            url: 'add',
            templateUrl: 'add.html',
            controller: 'SeriesCreateController',
        })
        .state('series.edit', {
            url: ':id/edit',
            templateUrl: 'update.html',
            controller: 'SeriesEditController',
        });
    // to not have this #/ in url use html5Mode
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
}]);

app
    .service('omdbApi', require('./services/omdbApi'))
    .service('SeriesApi', require('./services/seriesApi'))
    .service('Series', require('./services/series'))
    .controller('SeriesListController', require('./controllers/seriesList'))
    .controller('SeriesCreateController', require('./controllers/seriesCreate'))
    .controller('SeriesEditController', require('./controllers/seriesEdit'))
    .directive('series', require('./directives/series'));

module.exports = app;
// TODO:
// 1) webpack
// 2) regex
// 3) sass
// 4) docker: https://medium.freecodecamp.com/a-beginner-friendly-introduction-to-containers-vms-and-docker-79a9e3e119b#.n0keskiot
// 5) service tests
// 6) expand add and edit for each season
// 7) rewrite with react: https://github.com/dternyak/React-Redux-Flask
// Additional:
//   - grunt/gulp
//   - source maps
//   - more css: https://medium.freecodecamp.com/leveling-up-css-44b5045a2667#.kiyskqutd



