var $inject = [];

var series = function() {
    return {
        templateUrl: 'directives/series.html',
        replace: true,
        scope: {
            seriesObject: '=',
            deleteSeries: '&'
        },
        link: function(scope, elements, attrs, controller) {
            var $series = $(elements[0]);
            var $seriesButtons = $series.find('.series-buttons');
            $series
                .mouseenter(function() {
                    $seriesButtons.fadeIn(400);
                })
                .mouseleave(function() {
                    $seriesButtons.fadeOut(400);
                });
        }
    };
};

series.$inject = $inject;

module.exports = series;
