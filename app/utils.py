from marshmallow import validate, ValidationError

def get_string_validator(cls, attr):
    """Return marshmallow string length validator for a given sqlalchemy column"""
    return validate.Length(
        min=1, max=getattr(cls, attr).property.columns[0].type.length)

def must_not_be_blank(data):
    if not data:
        raise ValidationError('Missing data.')
