from app.models import Series, Season, Episode, Genre, db
from datetime import date, time, timedelta

def _add_genres():
    genres = [
        'Action', 'Animation', 'Comedy', 'Documentary', 'Family',
        'Film-Noir', 'Horror', 'Musical', 'Romance', 'Sport', 'War',
        'Adventure', 'Biography', 'Crime', 'Drama', 'Fantasy', 'History',
        'Music', 'Mystery', 'Sci-Fi', 'Thriller', 'Western',
    ]

    for genre in genres:
        obj = Genre(genre)
        db.session.add(obj)
    db.session.commit()

def _add_series():
    series1 = Series(
        title='Foo',
        description='FooDescription',
        actors='FooActor1, FooActor2',
        poster='./static/images/star.png',
        imdb_id='foo_imdb_id',
        rating=8.8,
        seasons=[
            Season(
                season_nr=1,
                air_time=time(20,0,0),
                episodes=[
                    Episode(
                        imdb_id='imdb_id_foo_ep1',
                        title='foo_ep1',
                        rating=9.2,
                        episode_nr=1,
                        air_date=date(2016,2,2)
                    ),
                    Episode(
                        imdb_id='imdb_id_foo_ep2',
                        title='foo_ep2',
                        rating=9.0,
                        episode_nr=2,
                        air_date=date(2016,2,9)
                    ),
                    Episode(
                        imdb_id='imdb_id_foo_ep3',
                        title='foo_ep3',
                        rating=9.3,
                        episode_nr=3,
                        air_date=date.today() - timedelta(days=1)
                    ),
                    Episode(
                        imdb_id='imdb_id_foo_ep4',
                        title='foo_ep4',
                        rating=9.7,
                        episode_nr=4,
                        air_date=date.today() + timedelta(days=3, hours=2)
                    )
                ]
            )
        ],
        genres=[Genre.query.get(1), Genre.query.get(2)]
    )
    series2 = Series(
        title='Bar',
        description='BarDescription',
        actors='BarActor1, BarActor2',
        poster='./static/images/star.png',
        imdb_id='bar_imdb_id',
        rating=2.0,
        seasons=[
            Season(
                season_nr=1,
                air_time=time(20,0,0),
                episodes=[
                    Episode(
                        imdb_id='imdb_id_bar_ep1',
                        title='bar_ep1',
                        rating=9.2,
                        episode_nr=1,
                        air_date=date(2016,2,2)
                    ),
                    Episode(
                        imdb_id='imdb_id_bar_ep2',
                        title='bar_ep2',
                        rating=9.0,
                        episode_nr=2,
                        air_date=date(2016,2,9)
                    )
                ]
            ),
            Season(
                season_nr=2,
                air_time=time(12,30,0),
                episodes=[
                    Episode(
                        imdb_id='imdb_id_bar_s02e01',
                        title='bar_s02e01',
                        rating=9.2,
                        episode_nr=1,
                        air_date=date(2016,2,10)
                    ),
                ]
            ),
        ],
        genres=[Genre.query.get(1)]
    )
    db.session.add(series1)
    db.session.add(series2)
    db.session.commit()

def initTestingDB(
    series=False,
    full_db=False,
):
    if series:
        _add_genres()
        _add_series()

