// This file is an entry point for angular tests
// Avoids some weird issues when using webpack + angular.

var context = require.context('../tests/js/', true, /\.js$/);

context.keys().forEach(context);
