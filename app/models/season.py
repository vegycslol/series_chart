from marshmallow import Schema, fields, validate
from app.models import db, get_string_validator, must_not_be_blank
from sqlalchemy import UniqueConstraint
from app.models.mixins import Base
from app.models.episode import EpisodeSchema

import datetime

class Season(Base):
    __tablename__ = 'seasons'
    __table_args__ = (
        UniqueConstraint('series_id', 'season_nr', name='series_season_uc'),
    )

    id = db.Column(db.Integer, primary_key=True)
    series_id = db.Column(db.Integer, db.ForeignKey('series.id'))
    season_nr = db.Column(db.Integer, nullable=False)
    episodes = db.relationship('Episode', backref='season', cascade='all, delete')
    air_time = db.Column(db.Time())


class SeasonSchema(Schema):

    id = fields.Int(dump_only=True)
    series_id = fields.Int()
    season_nr = fields.Int()
    air_time = fields.Time()
    episodes = fields.Nested(EpisodeSchema, many=True, validate=must_not_be_blank)

    class Meta:
        type_ = 'seasons'
