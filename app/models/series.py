from marshmallow import Schema, fields, post_dump, pre_dump, validate, ValidationError
from app.models import db, get_string_validator, must_not_be_blank
from app.models.mixins import Base, Rated, Titled
from app.models.season import SeasonSchema
from app.models.genre import GenreSchema
from app.models.episode import EpisodeSchema
from sqlalchemy.orm import backref

import datetime


genre_series = db.Table(
    'genre_series',
    db.Model.metadata,
    db.Column('series_id', db.Integer, db.ForeignKey('series.id')),
    db.Column('genre_id', db.Integer, db.ForeignKey('genres.id')),
)


class Series(Base, Titled, Rated):
    __tablename__ = 'series'

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(250), nullable=False)
    actors = db.Column(db.String(250), nullable=False)
    poster = db.Column(db.String(300), nullable=False)
    imdb_id = db.Column(db.String(20), unique=True, nullable=False)
    seasons = db.relationship('Season', backref='series', cascade='all')
    genres = db.relationship(
        'Genre',
        secondary=genre_series,
    )

    def add(self):
        db.session.add(self)
        db.session.commit()

    def update(self):
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def get_next_episode(self):
        """Returns the next episode if there is one, otherwise returns None"""
        episodes = sorted(
            [episode for season in self.seasons for episode in season.episodes
             if episode.air_date is not None],
            key=lambda x: x.air_date
        )
        today = datetime.date.today()
        for episode in episodes:
            if episode.air_date >= today:
                return episode
        return None


class SeriesSchema(Schema):

    id = fields.Int(dump_only=True)
    title = fields.Str(validate=get_string_validator(Series, 'title'))
    description = fields.Str(
        required=True,
        validate=get_string_validator(Series, 'description')
    )
    actors = fields.Str(
        required=True,
        validate=get_string_validator(Series, 'actors')
    )
    poster = fields.Str(
        required=True,
        validate=get_string_validator(Series, 'poster')
    )
    imdb_id = fields.Str(
        required=True,
        validate=get_string_validator(Series, 'imdb_id')
    )
    rating = fields.Float()
    seasons = fields.Nested(SeasonSchema, many=True, validate=must_not_be_blank)
    genres = fields.Nested(GenreSchema, many=True, validate=must_not_be_blank)

    next_episode = fields.Nested(EpisodeSchema, dump_only=True)

    @pre_dump
    def add_next_episode(self, data):
        """Add 'next_episode' attribute to Series model so it gets dumped"""
        setattr(data, 'next_episode', data.get_next_episode())
        return data

    class Meta:
        type_ = 'series'

