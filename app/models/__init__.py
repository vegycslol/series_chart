from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

from app.utils import get_string_validator, must_not_be_blank
from app.models.genre import Genre, GenreSchema
from app.models.series import Series, SeriesSchema
from app.models.season import Season, SeasonSchema
from app.models.episode import Episode, EpisodeSchema
