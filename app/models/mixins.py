from app.models import db


class Base(db.Model):
    __abstract__ = True

    @classmethod
    def create(cls, **kw):
        r = cls(**kw)
        db.session.add(r)
        return r

    def save(self):
        db.session.add(self)

    def delete(self):
        db.session.delete(self)

    def __repr__(self):
        values = ', '.join(
            ["{!s}={!r}".format(attr, getattr(self, attr))
             for attr in self.__table__.c.keys()]
        )
        return '{}({})'.format(self.__class__.__name__, values)


class Titled(object):

    title = db.Column(db.String(250))

    def by_title(self, name):
        return self.__class__.query.filter_by(title=name)


class Rated(object):

    rating = db.Column(db.Float())
