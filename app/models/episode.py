# from marshmallow_jsonapi import Schema, fields
from marshmallow import Schema, fields, validate, pre_dump
from app.models import db, get_string_validator
from sqlalchemy import UniqueConstraint
from app.models.mixins import Base, Rated, Titled


class Episode(Base, Titled, Rated):
    __tablename__ = 'episodes'
    __table_args__ = (
        UniqueConstraint('season_id', 'episode_nr', name='season_episode_uc'),
    )

    id = db.Column(db.Integer, primary_key=True)
    season_id = db.Column(db.Integer, db.ForeignKey('seasons.id'))
    imdb_id = db.Column(db.String(20), unique=True, nullable=False)
    episode_nr = db.Column(db.Integer, nullable=False)
    air_date = db.Column(db.Date())


class EpisodeSchema(Schema):
    id = fields.Int(dump_only=True)
    title = fields.Str(validate=get_string_validator(Episode, 'title'))
    imdb_id = fields.Str(
        required=True,
        validate=get_string_validator(Episode, 'imdb_id')
    )
    rating = fields.Float()
    episode_nr = fields.Int(required=True)
    air_date = fields.Date(allow_none=True)
    season_id = fields.Int()

    air_time = fields.Str(dump_only=True)

    @pre_dump
    def add_next_episode(self, data):
        """Add 'air_time' attribute to Episode model so it gets dumped"""
        setattr(data, 'air_time', str(data.season.air_time))
        return data
    class Meta:
        type_ = 'episodes'
