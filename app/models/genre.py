# from marshmallow_jsonapi import Schema, fields
from marshmallow import Schema, fields, validate
from flask_sqlalchemy import SQLAlchemy
from app.models import db


class Genre(db.Model):
    __tablename__ = 'genres'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)

    def __init__(self, name):
        self.name = name


class GenreSchema(Schema):

    id = fields.Int(dump_only=True)
    name = fields.Str()

    # class Meta:
    #     # fields = ('id', 'name')
    #     type_ = 'genres'

