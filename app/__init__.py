from app.models import Genre
from app.controller import series
from app.models import db
from flask import Flask

# http://flask.pocoo.org/docs/0.10/patterns/appfactories/
def create_app(config_filename):
    app = Flask(__name__, static_folder='static')
    app.config.from_object(config_filename)

    #Init Flask-SQLAlchemy
    app.register_blueprint(series, url_prefix='/api/v1/series')
    db.init_app(app)

    with app.app_context():
        # this is idempotent, it adds new tables but doesnt change
        # existing, use alembic for this
        db.create_all()
        genres = [
            'Action', 'Animation', 'Comedy', 'Documentary', 'Family',
            'Film-Noir', 'Horror', 'Musical', 'Romance', 'Sport', 'War',
            'Adventure', 'Biography', 'Crime', 'Drama', 'Fantasy', 'History',
            'Music', 'Mystery', 'Sci-Fi', 'Thriller', 'Western',
        ]

        for genre in genres:
            if Genre.query.filter(Genre.name == genre).first() is None:
                obj = Genre(genre)
                db.session.add(obj)
        db.session.commit()

    from flask import render_template, send_from_directory

    @app.route('/<path:filename>')
    def file(filename):
        from os import path
        return send_from_directory(path.join(app.root_path, 'templates'), filename)

    @app.route('/')
    def index():
        return render_template('index.html')

    return app
