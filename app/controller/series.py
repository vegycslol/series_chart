from flask import Blueprint, request, jsonify, make_response
from flask_restful import Api, Resource
from marshmallow import ValidationError
from sqlalchemy import and_
from sqlalchemy.exc import SQLAlchemyError, IntegrityError

from app.models import db
from app.models import Series, SeriesSchema
from app.models import Genre, GenreSchema
from app.models import Season, SeasonSchema
from app.models import Episode, EpisodeSchema

import datetime
import json
import logging

# logger for series
logger = logging.getLogger('series')
logger.setLevel(logging.ERROR)

#Initialize a Flask Blueprint,
series = Blueprint('series', __name__)

schema = SeriesSchema(strict=True)

#Initialize the  API  object using the Flask-RESTful API class
api = Api(series)

# Create CRUD classes using the Flask-RESTful Resource class
class CreateListSeries(Resource):

    def get(self):
        series_query = Series.query.all()
        # untill marshmallow jsonapi doesnt provide compound components
        # and jsonapi doesnt have a standard for nested models use custom json
        results = schema.dump(series_query, many=True).data
        # no need to jsonify, flask-restful takes care of that
        return { 'data': results }

    def post(self):
        raw_dict = request.get_json(force=True)
        try:
            # shorten the description if too long
            desc = raw_dict['description']
            if len(desc) > 250:
                desc = ' '.join(desc.split()[:-3]) + ' ...'
            raw_dict['genres'] = [
                {
                    'id': Genre.query.filter(Genre.name == genre.strip()).first().id,
                    'name': genre
                 }
                for genre in raw_dict['genres']
            ]
            schema.validate(raw_dict)
            # create models with correct datetime attrs
            seasons = []
            for season in raw_dict['seasons']:
                episodes = []
                for ep in season['episodes']:
                    if ep['air_date'] is not None:
                        ep['air_date'] = datetime.datetime.strptime(ep['air_date'], '%Y-%m-%d').date()
                    episodes.append(Episode(**ep))
                season['episodes'] = episodes
                season['air_time'] = datetime.datetime.strptime(season['air_time'],'%H:%M:%S').time()
                seasons.append(Season(**season))
            raw_dict['seasons'] = seasons
            raw_dict['genres'] = [Genre.query.get(genre['id']) for genre in raw_dict['genres']]
            series = Series(**raw_dict)
            series.add()
            # return created object in json
            query = Series.query.get(series.id)
            results = schema.dump(query).data
            return { 'data': results }, 201

        except ValidationError as err:
            return { 'error': err.messages }, 403

        except IntegrityError as e:
            return { 'error': 'This series is already in DB' }, 403

        except SQLAlchemyError as e:
            db.session.rollback()
            logger.error(str(e))
            return { 'error': 'Database problem' }, 403


class GetUpdateDeleteSeries(Resource):

    def get(self, id):
        series_query = Series.query.get_or_404(id)
        result = schema.dump(series_query).data
        for season in result['seasons']:
            season['air_time'] = '1970-01-01T' + season['air_time'] + '.000Z'
        return { 'data': result }

    def patch(self, id):
        # doesnt support changing seasons info
        # TODO: tests for new seasons, episodes...
        partial_schema = SeriesSchema(partial=True)
        series = Series.query.get_or_404(id)
        raw_dict = request.get_json(force=True)['data']
        try:
            # change genres representation for schema.load
            raw_dict['genres'] = [
                { 'name': genre }
                for genre in raw_dict['genres']
            ]
            partial_schema.validate(raw_dict)
            for key, value in partial_schema.load(raw_dict).data.items():
                if key == 'genres':
                    # change value to be a proper list of Genre objects
                    value = [
                        Genre.query.filter(Genre.name == genre['name'].strip()).one()
                        for genre in value
                    ]
                if key == 'seasons':
                    for season in value:
                        cur_season = Season.query.filter(and_(Season.season_nr==season['season_nr'], Season.series_id==series.id)).one_or_none()
                        # this season isnt in the db yet, create it
                        if cur_season is None:
                            # and add it TODO: check this crap
                            episodes = []
                            for ep in season['episodes']:
                                episodes.append(Episode(**ep))
                            season['episodes'] = episodes
                            series.seasons.append(Season(**season))
                            continue
                        # update seasons episodes
                        for ep in season['episodes']:
                            cur_ep = Episode.query.filter_by(imdb_id = ep['imdb_id']).one_or_none()
                            if cur_ep is None:
                                cur_season.episodes.append(Episode(**ep))
                                continue
                            for attr, val in ep.items():
                                setattr(cur_ep, attr, val)
                        # update seasons other attributes
                        for attr, val in (x for x in season.items() if x[0] != 'episodes'):
                            setattr(cur_season, attr, val)
                    continue

                setattr(series, key, value)

            series.update()
            return self.get(id)

        except ValidationError as err:
            return { 'error': err.messages }, 401

        except SQLAlchemyError as e:
            db.session.rollback()
            logger.error(str(e))
            return { 'error': 'Database problem' }, 401

    def delete(self, id):
        series = Series.query.get_or_404(id)
        try:
            delete = series.delete()
            response = make_response()
            response.status_code = 204
            return response

        except SQLAlchemyError as e:
            db.session.rollback()
            logger.error(str(e))
            return { 'error': 'Database problem' }, 401


class GetSeriesGenre(Resource):

    def get(self, id):
        series_query = Series.query.get_or_404(id)
        data, errors = GenreSchema(many=True).dump(series_query.genres)
        return { 'data': data }


# Map classes to API enpoints
api.add_resource(CreateListSeries, '/')
api.add_resource(GetUpdateDeleteSeries, '/<int:id>')
api.add_resource(GetSeriesGenre, '/<int:id>/genres')
