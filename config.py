mysql_db_username = 'root'
mysql_db_password = ''
mysql_db_name = 'series_chart'
mysql_db_hostname = 'localhost'
SQLALCHEMY_TRACK_MODIFICATIONS = True

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{DB_USER}:{DB_PASS}@{DB_ADDR}/{DB_NAME}'.format(
    DB_USER=mysql_db_username,
    DB_PASS=mysql_db_password,
    DB_ADDR=mysql_db_hostname,
    DB_NAME=mysql_db_name,
)
